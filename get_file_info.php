<?php
set_time_limit(0);

header('Content-Type: application/json');

$data = [];
$status = 200;

try {
    if (isset($_GET['url']) && !empty($_GET['url'])) {
        $data = get_headers($_GET['url'], true);
    } else {
        $status = 404;
    }
} catch (\Exception $e) {
    $status = 500;
}

http_response_code($status);

echo json_encode(
    $data
);