<?php
set_time_limit(0);

$data = null;
$status = 200;

try {
    if (isset($_GET['url']) && !empty($_GET['url'])) {
        $data = file_get_contents($_GET['url']);
    } else {
        $status = 404;
    }
} catch (\Exception $e) {
    $status = 500;
}

http_response_code($status);

echo $data;